# My Notes to markdown

Converts unzipped encrypted backup from My Notes android app (by KreoSoft) to markdown files. Each note to its own file.

# Requirements
* Linux (faking creation time is not portable)
* [NodeJS 12+](https://nodejs.org)
* `unshare` which supports `--time`

# Installation

```sh
$ npm i -g
```

```sh
$ sudo my-notes-to-markdown --help
```

```
Usage: my-notes-to-markdown [args] <input-json-file>
Converts unzipped encrypted backup from My Notes android app (by KreoSoft) to
markdown files. Each note to its own file.

Options:
      --version            Show version number                         [boolean]
  -d, --output-directory   Output directory                     [default: "out"]
  -v, --verbose            Show more debug logs                          [count]
  -q, --quiet              Turn the output to minimum                  [boolean]
  -s, --save-decoded-json  Save decoded data as a JSON file             [string]
  -h, --help               Show help                                   [boolean]

Examples:
  sudo my-notes-to-markdown notes\#1.json
  sudo my-notes-to-markdown --save-decoded-json decoded_notes.json --verbose
  --output-directory out2 notes\#1.json --quiet

Copyright (C) 2022 by monnef
This program is released under free GPL-3.0+ license.
```

# Supported note fields

* title
* content
* creation date
* modification date

# License
GPL-3.0+
