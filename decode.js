#!/usr/bin/env node

const fs = require('fs');
const { execSync } = require('child_process');
const { userInfo } = require('os');
const yargs = require('yargs');
const chalk = require('chalk');

const packageData = require('./package.json');

if (userInfo().uid !== 0) {
    console.error('This program must be run under root (to fake creation times).');
    process.exit(1);
}

const argv = yargs
    .scriptName(packageData.name)
    .usage(`Usage: $0 [args] <input-json-file>\n${packageData.description}`)
    .example('sudo $0 notes\\#1.json')
    .example('sudo $0 --save-decoded-json decoded_notes.json --verbose --output-directory out2 notes\\#1.json --quiet')

    .alias('d', 'output-directory')
    .default('d', 'out')
    .describe('d', 'Output directory')

    .count('verbose')
    .alias('v', 'verbose')
    .describe('v', 'Show more debug logs')

    .alias('q', 'quiet')
    .boolean('q')
    .describe('q', 'Turn the output to minimum')

    .string('s')
    .alias('s', 'save-decoded-json')
    .describe('s', 'Save decoded data as a JSON file')

    .alias('h', 'help')

    .demandCommand(1, 'Missing input JSON file.')
    .epilog(
        'Copyright (C) 2022 by monnef\n' +
        'This program is released under free GPL-3.0+ license.',
    )
    .argv

const verboseLevel = argv.verbose;
const outDir = argv.outputDirectory;

const genLog = lvl => (...args) => {
    if (verboseLevel >= lvl) console.log(...args);
};
const [logV, logVV] = [1, 2].map(genLog);
const log = (...args) => {
    if (!argv.quiet) console.log(...args);
}

logV('Arguments: ', argv);

logV('outDir: ', outDir);
if (!fs.existsSync(outDir)) {
    logV('Output directory is missing, creating.');
    fs.mkdirSync(outDir);
}

let data;
try {
    data = JSON.parse(fs.readFileSync(argv._[0]));
} catch (e) {
    console.error('Failed to load input data.')
    throw e;
}
logVV('Data: ', data);

const decryptValue = x => x.slice(2).split('\n').map(x => Buffer.from(x, 'base64').map(x => ~x).toString()).join('');

const decryptValuePair = x => ({ ...x, v: decryptValue(x.v) });

const decryptItem = x => ({ ...x, title: decryptValuePair(x.title), content: decryptValuePair(x.content) });

const decryptAll = xs => xs.map(decryptItem);

const res = decryptAll(data).sort((a, b) => a.dateUpdated.v - b.dateUpdated.v);

logVV('Decrypted data:\n', res);
if (argv.saveDecodedJson) {
    fs.writeFileSync(argv.saveDecodedJson, JSON.stringify(res, null, 2));
}
const myNotesTimeToEpoch = x => x.toString().slice(0, -3) + '.' + x.toString().slice(-3);
const genProcessingNoteItemText = (tag, str) => chalk.cyan(tag) + ': ' + chalk.yellow(str);

res.forEach(x => {
    const title = x.title.v;
    const path = outDir + '/' + title + '.md';
    const creationTime = x.dateCreated.v;
    const content = x.content.v;
    const updateTime = x.dateUpdated.v;

    log(`\n🗒️ Processing note: ${chalk.cyanBright.bold(title)}`);
    log([
        genProcessingNoteItemText('path', path),
        genProcessingNoteItemText('creationTime', new Date(creationTime)),
        genProcessingNoteItemText('updateTime', new Date(updateTime)),
        genProcessingNoteItemText('content length', content.length),
    ].map(x => chalk.gray('|   ') + x).join('\n'));

    if (fs.existsSync(path)) fs.unlinkSync(path);

    const createFileWithDateCmd = `unshare --time sh -c 'id && date -s @${myNotesTimeToEpoch(creationTime)} && touch "${path}"'`;
    execSync(createFileWithDateCmd);

    fs.writeFileSync(path, content);

    fs.utimesSync(path, myNotesTimeToEpoch(updateTime), myNotesTimeToEpoch(updateTime));
    log(chalk.greenBright('Done') + '.');
});
